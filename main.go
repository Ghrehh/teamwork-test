package main

import (
	"gitlab.com/Ghrehh/teamwork-test/customers"

	"encoding/csv"
	"os"
	"log"
	"fmt"
)

func main() {
	f, err := os.Open("customers.csv")
	r := csv.NewReader(f)

	if err != nil {
		log.Fatal(err)
	}

	domains := customers.ParseDomains(r)
	fmt.Println(domains)
}
