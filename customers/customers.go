package customers

import (
	"encoding/csv"
	"strings"
	"log"
	"io"
)

const unparsableDomain string = "Unparsable Domain"

func getDomain(emailAddress string) string {
	segments := strings.Split(emailAddress, "@")

	if len(segments) == 2 {
		return segments[1]
	}

	return unparsableDomain
}

func ParseDomains(r *csv.Reader) map[string]int {
	m := make(map[string]int)

	for i := 0;; i++ {
		record, err := r.Read()

		if err == io.EOF {
			break
		} else if err != nil {
			log.Fatal(err)
		}

		// Skip Header. Couldn't find a clean way to do this without using `ReadAll` which
		// would load the entire file.
		if i == 0 {
			continue
		}

		email := record[2]
		m[getDomain(email)] += 1
	}

	return m
}
