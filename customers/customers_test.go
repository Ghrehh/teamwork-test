package customers

import (
	"testing"
	"reflect"
	"encoding/csv"
	"strings"
)

func TestGetDomain(t *testing.T) {
	tests := map[string]string{
		"foo@example.com": "example.com",
		"giberish": unparsableDomain,
	}

	for email, expectedResult := range tests {
		result := getDomain(email)

		if result != expectedResult {
			t.Error("did not format the provided email address correctly")
		}
	}
}

func TestParseDomains(t *testing.T) {
	fileContent := strings.NewReader(
		`first_name,last_name,email,gender,ip_address
		foo,bar,foo@bar.com,female,1.2.3.4.5
		bar,baz,baz@bar.com,male,1.2.3.4.5
		fizz,baz,fizz@buzz.com,male,1.2.3.4.5
		bar,foo,nonsense,male,1.2.3.4.5`)

	fakeCSV := csv.NewReader(fileContent)

	result := ParseDomains(fakeCSV)
	expectedResult := map[string]int{
		"bar.com": 2,
		"buzz.com": 1,
		unparsableDomain: 1,
	}

	if !reflect.DeepEqual(result, expectedResult) {
		t.Error("did not parse the provided file correctly")
	}
}
