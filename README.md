The instructions were a little hazy so I did what I thought was correct:

- Placed the business logic of parsing, formatting and returning the data in to
  the `customers` module. This allows it to be easily extracted into its own
  package/repo if needs be.

- Chose to not couple the provided csv file with that module, this way it can be
  passed any correctly formatted csv data by the file importing it. The provided
  `main.go` handles opening the file here.
